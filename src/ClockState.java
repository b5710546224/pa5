import java.util.Timer;

/** 
 * The class of state machine contain all of working state
 * for our alarm clock
 *  @author Natcha Pongsupanee 5710546224
 */
public class ClockState {
	/** the state that change to each of state depend on condition and working */
	State state;
	
	/** the gui display our clock */
	static ClockGUI gui;
	
	/** timer to update the clock */
	static Timer timer;

	/** 
	 * a construntor for new ClockState
	 * @param gui to display our clock
	 * 		  timer to update the clock
	 */
	public ClockState(ClockGUI gui,Timer timer){
		this.gui = gui;
		this.timer = timer;
		state = TIME_MODE;
	}
	
	/** 
	 * method to set state depend of the working
	 * @param new state the we want to set
	 */
	public void setState(State newstate){
		if(newstate!=state) newstate.enterState();
		this.state = newstate;
	}
	
	/** state when we displaying alarm mode */
	State ALARM_MODE = new State(){
		public void handleChar(String button) {
			if(button.equalsIgnoreCase("set")||button.equalsIgnoreCase("plus")||button.equalsIgnoreCase("minus"))
				setState(SET_HOUR);
		}

		public void enterState() {
			System.out.println("entered ALARM MODE");
			gui.setAlarm();
		}
	};
	
	/** state when we setting alarm hour */
	State SET_HOUR = new State(){
		public void handleChar(String button) {
			if(button.equalsIgnoreCase("plus")||button.equalsIgnoreCase("minus")){
				gui.setCurrentAlarmHour(button);
				gui.changeColor("hour");
			}
			else if(button.equalsIgnoreCase("set"))setState(SET_MIN);
		}
		public void enterState() {
			System.out.println("entered SET HOUR");
			gui.setAlarm();
			gui.changeColor("hour");
		}
	};
	
	/** state when we setting alarm minute */
	State SET_MIN = new State(){
		public void handleChar(String button) {
			if(button.equalsIgnoreCase("plus")||button.equalsIgnoreCase("minus")){
				gui.setCurrentAlarmMin(button);
				gui.changeColor("min");
			}
			else if(button.equalsIgnoreCase("set"))setState(SET_SEC);
		}
		public void enterState() {
			System.out.println("entered SET MIN");
			gui.setAlarm();
			gui.changeColor("min");
		}
	};
	
	/** state when we setting alarm second */
	State SET_SEC = new State(){
		public void handleChar(String button) {
			if(button.equalsIgnoreCase("plus")||button.equalsIgnoreCase("minus")){
				gui.setCurrentAlarmSec(button);
				gui.changeColor("sec");
			}
			else if(button.equalsIgnoreCase("set")){
				gui.resume();
				setState(TIME_MODE);
			}
		}
		public void enterState() {
			System.out.println("entered SET SEC");
			gui.setAlarm();
			gui.changeColor("sec");
		}
	};
	
	/** state when alarm is alerting */
	State RINGING = new State(){
		public void handleChar(String button) {
			if(button.equalsIgnoreCase("set")||button.equalsIgnoreCase("plus")||button.equalsIgnoreCase("minus")){
				gui.turnOffAlernt();
				setState(TIME_MODE);
			}
		}
		public void enterState(){
			gui.turnOnAlernt();
		}
	};
	
	/** state when we are displaying normal clock mode */
	State TIME_MODE = new State(){
		public void handleChar(String button) {
			if(button.equalsIgnoreCase("set")||button.equalsIgnoreCase("plus")){
				gui.pause();
				setState(ALARM_MODE);
			}
		}
		public void enterState(){
			System.out.println("entered TIME_MODE");
		}
	};
	
	/** 
	 * method to control state
	 * @param the button that was pressed in each of times
	 */
	public void clockState(String button){
		if(button.equalsIgnoreCase("alarm")) setState(RINGING);
		else state.handleChar(button);
	}
}
