/**
 * Interface of state machine using for controlling alarm clock.
 * @author Natcha Pongsupanee 5710546224
 */
public interface State {
	
	/**
	 * working in each of state
	 */
	public void handleChar(String button);
	
	/**
	 * Do this when enter the state
	 */
	public void enterState();
}
