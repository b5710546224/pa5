import java.io.BufferedInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * The class that using for play alarm alernt
 * @author Natcha Pongsupanee 5710546224
 */
public class AlarmRinging {
	/**
	 * method to play music when itself be called
	 */
	public void playSound(){
		try{
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(
				    new BufferedInputStream(getClass().getResourceAsStream("ringtone/ringtone1.wav"))));
			clip.start();
		}catch(Exception e){
		}
	}
}
