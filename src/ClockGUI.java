import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/** 
 * This GUI class plain how is our clock looks like
 *  @author Natcha Pongsupanee 5710546224
 */
public class ClockGUI extends JFrame{
	/** the variable to keep interval value */
	final static long INTERVAL = 500;
	
	/** global variable for all JLabel */
	private JLabel hourLabel1,hourLabel2,minLabel1,minLabel2,secLabel1,secLabel2,space1,space2;
	
	/** global variable for all JPanel */
	private JPanel mainPane,pane1,pane2;
	
	/** global variable for all JButton */
	private JButton setButton,plusButton,minusButton;
	
	/** variable to keep current time depend on PC time zone */
	protected static Date currentTime;
	
	/** the state of this clock */
	protected ClockState state;
	
	/** variable to keep the time to alernt alarm */
	protected Date currentAlarm;
	
	/** Timer to update our working */
	static Timer timer,alerntTimer;
	
	/** variable to play music for alarm alernt */
	protected AlarmRinging ringing;
	
	/** 
	 * the constructor for new ClockGUI
	 * @param timer to update TIME_MODE
	 * 		  clocktask is the function of timer
	 */
	public ClockGUI(Timer timer,TimerTask clocktask){
		initComponent();
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.timer = timer;
		state = new ClockState(this,timer);
		currentAlarm = new Date(0,0,0);
		ringing = new AlarmRinging();
	}
	
	/** 
	 * method to display GUI MODE
	 */
	public void initComponent(){
		currentTime = new Date();
		pane1 = new JPanel();
		pane1.setLayout(new FlowLayout());
		pane1.setBackground(Color.black);
		mainPane = new JPanel();
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		
		hourLabel1 = new JLabel();
		hourLabel2 = new JLabel();
		space1 = new JLabel(" : ");
		space1.setFont(new Font("Tahoma",Font.BOLD,18)); 
		space1.setForeground(Color.cyan);
		minLabel1 = new JLabel();
		minLabel2 = new JLabel();
		space2 = new JLabel(" : ");
		space2.setFont(new Font("Tahoma",Font.BOLD,18)); 
		space2.setForeground(Color.cyan);
		secLabel1 = new JLabel();
		secLabel2 = new JLabel();
		update();
		
		pane1.add(hourLabel1);
		pane1.add(hourLabel2);
		pane1.add(space1);
		pane1.add(minLabel1);
		pane1.add(minLabel2);
		pane1.add(space2);
		pane1.add(secLabel1);
		pane1.add(secLabel2);
		
		pane2 = new JPanel();
		pane2.setLayout(new FlowLayout());
		pane2.setBackground(Color.black);
		
		setButton = new JButton(" set ");
		setButton.setBackground(Color.black);
		setButton.setForeground(Color.cyan);
		plusButton = new JButton("  +  ");
		plusButton.setBackground(Color.black);
		plusButton.setForeground(Color.cyan);
		minusButton = new JButton("  -  ");
		minusButton.setBackground(Color.black);
		minusButton.setForeground(Color.cyan);
		
		ActionListener setButListener = new SetListener();
		setButton.addActionListener(setButListener);
		
		ActionListener plusButListener = new PlusListener();
		plusButton.addActionListener(plusButListener);
		
		ActionListener minusButListener = new MinusListener();
		minusButton.addActionListener(minusButListener);
		
		pane2.add(setButton);
		pane2.add(plusButton);
		pane2.add(minusButton);
		
		mainPane.add(pane1);
		mainPane.add(pane2);
		
		super.add(mainPane);
		this.pack();
		super.setVisible(true);
	}
	
	/** 
	 * method to update the number of time in TIME_MODE
	 */
	public void update(){
		hourLabel1 = addImage(hourLabel1,currentTime.getHours()/10+".png");
		hourLabel2 = addImage(hourLabel2,currentTime.getHours()%10+".png");
		minLabel1 = addImage(minLabel1,currentTime.getMinutes()/10+".png");
		minLabel2 = addImage(minLabel2,currentTime.getMinutes()%10+".png");
		secLabel1 = addImage(secLabel1,currentTime.getSeconds()/10+".png");
		secLabel2 = addImage(secLabel2,currentTime.getSeconds()%10+".png");
	}
	
	/** 
	 * method to set the color of number when we are setting alarm time.
	 * depend on the state the we are in
	 * @param state is string tell us which state we are in
	 */
	public void changeColor(String stage){
		if(stage.equalsIgnoreCase("hour")){
			hourLabel1 = addImage(hourLabel1,currentAlarm.getHours()/10+"r.png");
			hourLabel2 = addImage(hourLabel2,currentAlarm.getHours()%10+"r.png");
		}else if(stage.equalsIgnoreCase("min")){
			minLabel1 = addImage(minLabel1,currentAlarm.getMinutes()/10+"r.png");
			minLabel2 = addImage(minLabel2,currentAlarm.getMinutes()%10+"r.png");
		}else{
			secLabel1 = addImage(secLabel1,currentAlarm.getSeconds()/10+"r.png");
			secLabel2 = addImage(secLabel2,currentAlarm.getSeconds()%10+"r.png");
		}
	}
	
	/** 
	 * method to set GUI display when we are in ALARM_MODE
	 */
	public void setAlarm(){
		hourLabel1 = addImage(hourLabel1,currentAlarm.getHours()/10+"y.png");
		hourLabel2 = addImage(hourLabel2,currentAlarm.getHours()%10+"y.png");
		minLabel1 = addImage(minLabel1,currentAlarm.getMinutes()/10+"y.png");
		minLabel2 = addImage(minLabel2,currentAlarm.getMinutes()%10+"y.png");
		secLabel1 = addImage(secLabel1,currentAlarm.getSeconds()/10+"y.png");
		secLabel2 = addImage(secLabel2,currentAlarm.getSeconds()%10+"y.png");
	}
	
	/** 
	 * method to call ringing to play aleart music
	 */
	public void alarmAlernt(){
		ringing.playSound();
	}
	
	/** 
	 * method to set Alarm hour
	 * @param the button to tell us to plus or minus
	 */
	public void setCurrentAlarmHour(String button){
		if(button.equalsIgnoreCase("plus")){
			if(currentAlarm.getHours()==23)currentAlarm.setHours(0);
			else currentAlarm.setHours(currentAlarm.getHours()+1);
		}else{
			if(currentAlarm.getHours()==0)currentAlarm.setHours(23);
			else currentAlarm.setHours(currentAlarm.getHours()-1);
		}
	}
	
	/** 
	 * method to set Alarm minute
	 * @param the button to tell us to plus or minus
	 */
	public void setCurrentAlarmMin(String button){
		if(button.equalsIgnoreCase("plus")){
			if(currentAlarm.getMinutes()==59)currentAlarm.setMinutes(0);
			else currentAlarm.setMinutes(currentAlarm.getMinutes()+1);
		}else{
			if(currentAlarm.getMinutes()==0)currentAlarm.setMinutes(59);
			else currentAlarm.setMinutes(currentAlarm.getMinutes()-1);
		}
	}
	
	/** 
	 * method to set Alarm second
	 * @param the button to tell us to plus or minus
	 */
	public void setCurrentAlarmSec(String button){
		if(button.equalsIgnoreCase("plus")){
			if(currentAlarm.getSeconds()==59)currentAlarm.setSeconds(0);
			else currentAlarm.setSeconds(currentAlarm.getSeconds()+1);
		}else{
			if(currentAlarm.getSeconds()==0)currentAlarm.setSeconds(59);
			else currentAlarm.setSeconds(currentAlarm.getSeconds()-1);
		}
	}
	
	/** 
	 * method to set image for each of Label
	 * @param label that we want to set
	 * 		  name is the name of picture
	 */
	public JLabel addImage(JLabel label,String name){
		ClassLoader loader = this.getClass().getClassLoader();
		URL url = loader.getResource("images/"+name);
		ImageIcon imag = new ImageIcon(url);
		label.setIcon(imag);
		return label;
	}
	
	/** 
	 * method to resume TIME_MODE
	 */
	public void resume() {
		long delay = 1000 - System.currentTimeMillis()%1000;
	    this.timer = new Timer();
	    this.timer.schedule(this.new ClockTask(),delay,INTERVAL);
	}
	
	/** 
	 * method to pause TIME_MODE
	 */
	public void pause() {
	    this.timer.cancel();
	}
	
	/** 
	 * method to turn on alarm alernt
	 */
	public void turnOnAlernt(){
		this.alerntTimer = new Timer();
		this.alerntTimer.schedule(this.new Alernt(),0,2600);
	}
	
	/** 
	 * method to turn on alarm alernt
	 */
	public void turnOffAlernt(){
		this.alerntTimer.cancel();
	}
	
	/** 
	 * class Alernt the extends Timer Task used to call alarmAllernt to play music
	 */
	class Alernt extends TimerTask {
		public void run(){
			alarmAlernt();
		}
	}
	
	/** 
	 * class ClockTask the extends Timer Task used to update current time and display of GUI
	 */
	class ClockTask extends TimerTask {
		public void run() {
			currentTime.setTime(System.currentTimeMillis());
			if(currentTime.getHours()==currentAlarm.getHours()&&
			   currentTime.getMinutes()==currentAlarm.getMinutes()&&
			   currentTime.getSeconds()==currentAlarm.getSeconds()){
				state.clockState("alarm");
			}
			update();
		}
	};
	
	/** 
	 * class action listener for set button
	 */
	class SetListener implements ActionListener {
		/** method to perform action when set the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			state.clockState("set");
		}
	};
	
	/** 
	 * class action listener for plus button
	 */
	class PlusListener implements ActionListener {
		/** method to perform action when the plus button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			state.clockState("plus");
		}
	};	
	
	/** 
	 * class action listener for minus button
	 */
	class MinusListener implements ActionListener {
		/** method to perform action when the minus button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			state.clockState("minus");
		}
	};
}
