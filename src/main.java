import java.util.Timer;
import java.util.TimerTask;
/** 
 * class main of this clock 
 * @author Natcha Pongsupanee 5710546224
 */
public class main {
	final static long INTERVAL = 500;
	static ClockGUI gui;
	static Timer timer;
	static TimerTask clocktask;
	
	/** 
	 * main method
	 */
	public static void main(String[]args){
		timer = new Timer();
		gui = new ClockGUI(timer,clocktask);
		clocktask = gui.new ClockTask();
		long delay = 1000 - System.currentTimeMillis()%1000;
		timer.scheduleAtFixedRate( clocktask, delay, INTERVAL );	
	}
}
